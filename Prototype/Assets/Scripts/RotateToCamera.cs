﻿using UnityEngine;
using System.Collections;

public class RotateToCamera : MonoBehaviour
{

    // public Transform target;
    public float speed;
    SpawnController controller = new SpawnController();

    Vector3 targetDir = new Vector3(0, 0, 0);

    /* void Start()
     {
         //Vector3 targetDir = target.position - transform.position;
         float step = speed * Time.deltaTime;
         Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
         Debug.DrawRay(transform.position, newDir, Color.red);
         transform.rotation = Quaternion.LookRotation(newDir);
     }*/

    public GameObject target;

    void Update()
    {
        var n = targetDir - transform.position;
        transform.rotation = Quaternion.LookRotation(n);
    }

    void OnTriggerEnter(Collider otherObj)
    {
    }
}