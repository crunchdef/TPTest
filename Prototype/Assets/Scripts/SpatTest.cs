﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class SpatTest : MonoBehaviour {

    public GameObject ball;
    public GameObject instructions;
    Text intrText;
    Rotate rotateObj;
    //int listenerOffset = 1;

    int testnumber;
    int[] azs   =   {    0,    90,    180,    -90,    45,    -45,   75,  -75,    0};
    int[] elevs =   {    0,     0,      0,      0,   -30,    -30,  -30,  -30,  -90};
    float[] dists = { 1.5f,  1.5f,   1.5f,   1.5f,  1.5f,   1.5f, 1.5f, 1.5f, 1.5f};

    // Use this for initialization
    void Start ()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "SpatTest")
        {
            UnityEngine.VR.VRSettings.enabled = false;
        }
        // spawn a ball directly in front of listener 1 metre distance
        rotateObj = new Rotate();
        intrText = instructions.GetComponent<Text>();

        Vector3 position = new Vector3(0, 0, 0);
        StartCoroutine(changeSrcPos());
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}



    IEnumerator WaitForInput(string buttonName)
    {
        while (!Input.GetButtonDown(buttonName))
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.2f);
    }

    IEnumerator changeSrcPos()
    {
        GameObject soundSource = Instantiate(ball, rotateObj.convertAngle(azs[0]+rotateObj.azimuthOffset, elevs[0]+rotateObj.elevationOffset, dists[0]), Quaternion.identity) as GameObject;
        for (int i = 0; i < azs.Length; i++)
        {
            intrText.text = Convert.ToString(i + 1);
            soundSource.transform.position = rotateObj.convertAngle(azs[i]+rotateObj.azimuthOffset, elevs[i]+rotateObj.elevationOffset, dists[i]);
            yield return StartCoroutine(WaitForInput("Fire1"));
        }
        intrText.text = "Done";
        yield return StartCoroutine(WaitForInput("Fire1"));
        SceneManager.LoadScene("LevelSelect", LoadSceneMode.Single);
        yield return null;
    }

   /* Vector3 convertAngle(float azimuth, float elevation, float distance)
    {
        azimuth = convertToRadians(azimuth);
        elevation = convertToRadians(elevation);


        Vector3 newPosition;

        newPosition.x = distance * Mathf.Sin(elevation) * Mathf.Cos(azimuth);
        //Debug.Log("X: " + newPosition.x);
        newPosition.z = distance * Mathf.Sin(elevation) * Mathf.Sin(azimuth);
        //Debug.Log("Z: " + newPosition.z);
        newPosition.y = (distance * Mathf.Cos(elevation)) + listenerOffset;
        //Debug.Log("Y: " + newPosition.y);

        //Debug.Log(newPosition);
        return newPosition;
    }

    float convertToRadians(float angle)
    {
        float radian = angle * (Mathf.PI / 180);
        return radian;

    }*/
}
