using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (CharacterController))]
    [RequireComponent(typeof (AudioSource))]
    public class FirstPersonController : MonoBehaviour
    {
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private MouseLook m_MouseLook;
    
        private Camera m_Camera;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private Vector3 m_OriginalCameraPosition;
        private AudioSource m_AudioSource;
        // True = use mouse for camera movement
        // False = use mouse for the sliders
        private bool mouseMode;

        // Use this for initialization
        private void Start()
        {
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            m_OriginalCameraPosition = m_Camera.transform.localPosition;
            m_AudioSource = GetComponent<AudioSource>();
			m_MouseLook.Init(transform , m_Camera.transform);
            mouseMode = true;
            Cursor.lockState = CursorLockMode.None;
        }


        // Update is called once per frame
        private void Update()
        {
            /* Check whether the right mouse button is clicked.
             * Toggle between cursor being visible and used for menus
             * and used for camera orientation
             */
            //Cursor.lockState = CursorLockMode.None;
            Cursor.visible = (!mouseMode);
            if (Input.GetButtonDown("Fire2"))
            {
                if (mouseMode == true)
                {
                    mouseMode = false;
                    Cursor.visible = true;
                }
                else
                {
                    mouseMode = true;
                    Cursor.visible = false;
                }
            }
            
            if(mouseMode == true)
            {
                RotateView();
            }
           
        }


        private void FixedUpdate()
        {
            float speed;
            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = transform.forward*m_Input.y + transform.right*m_Input.x;

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                               m_CharacterController.height/2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;


            //m_CollisionFlags = m_CharacterController.Move(m_MoveDir*Time.fixedDeltaTime);

            //m_MouseLook.UpdateCursorLock();
        }


        private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition;
            
            newCameraPosition = m_Camera.transform.localPosition;
            newCameraPosition.y = m_OriginalCameraPosition.y;

            m_Camera.transform.localPosition = newCameraPosition;
        }

        private void RotateView()
        {
            m_MouseLook.LookRotation (transform, m_Camera.transform);
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }
    }
}
