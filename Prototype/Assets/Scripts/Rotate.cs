﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Rotate : MonoBehaviour {

    // this script is on the football object (in case I can't find it again
    // It gets the sliders info and then changes the football's position
    // the sliders are got by .find ... yeah I know

    private GameObject userCam;

    private Transform target;
    private Vector3 movementVector;
    private float movementSpeed =8;
    private CharacterController characterController;

    GameObject azimuthSlider;
    public Slider azimuth;
    Text azimuthValue;
    GameObject elevationSlider;
    public Slider elevation;
    Text elevationValue;
    GameObject distanceSlider;
    public Slider distance;
    Text distanceValue;

    //listener offset adjusts the source position to account for position of the listener so it rotates around the listner and not 0,0,0
    public int listenerOffset = 1;
    //0 on the azimuth is off to the right this offsets to the front.
    public int azimuthOffset = 90;
    // brings 0 to no elevation and 90 to directly above
    public int elevationOffset = 90;


    int repeat = 0;

    void Start () {
        userCam = GameObject.FindGameObjectWithTag("MainCamera");
        target = userCam.transform;

        azimuthSlider = GameObject.Find("Azimuth");
        azimuth = azimuthSlider.GetComponent<Slider> ();

        //azimuthValue = azimuthSlider.GetComponent<Text>();
        azimuth.onValueChanged.AddListener (delegate {ValueChangeCheck ();});

        elevationSlider = GameObject.Find("Elevation");
        elevation = elevationSlider.GetComponent<Slider>();

        //elevationValue = elevationSlider.GetComponent<Text>();
        elevation.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        distanceSlider = GameObject.Find("Distance");
        distance = distanceSlider.GetComponent<Slider>();
        //distanceValue = distanceSlider.GetComponent<Text>();
        distance.onValueChanged.AddListener(delegate { ValueChangeCheck(); });

        for (int i = 0; i < azimuthSlider.transform.childCount; i++)
        {
            if (azimuthSlider.transform.GetChild(i).transform.name == "ValueText")
            {
                Debug.Log(azimuthSlider.transform.GetChild(i).transform.name);
                azimuthValue = azimuthSlider.transform.GetChild(i).transform.GetComponent<Text>();
            }
        }

        for (int i = 0; i < elevationSlider.transform.childCount; i++)
        {
            if (elevationSlider.transform.GetChild(i).transform.name == "ValueText")
            {
                elevationValue = elevationSlider.transform.GetChild(i).transform.GetComponent<Text>();
            }
        }

        for (int i = 0; i < distanceSlider.transform.childCount; i++)
        {
            if (distanceSlider.transform.GetChild(i).transform.name == "ValueText")
            {
                distanceValue = distanceSlider.transform.GetChild(i).transform.GetComponent<Text>();
            }
        }

        azimuthValue.text = azimuth.value.ToString("0.0");
        elevationValue.text = elevation.value.ToString("0.0");
        distanceValue.text = distance.value.ToString("0.0");
        transform.position = convertAngle(azimuth.value + azimuthOffset, elevation.value + elevationOffset, distance.value);
    }
	

	void Update () {
        //transform.LookAt(target);

        movementVector.x = -(Input.GetAxis("LeftJoystickX") * movementSpeed);
        movementVector.y = -(Input.GetAxis("LeftJoystickY") * movementSpeed);
        movementVector.z = (Input.GetAxis("RightJoystickY") * movementSpeed);
        transform.Translate((movementVector * Time.deltaTime)); 
        
    }
	
    public void ValueChangeCheck()
	{
        //float temp = azimuth.value * -1;
        //azimuthValue.text = temp.ToString("0.0");
        azimuthValue.text = azimuth.value.ToString("0.0");
        elevationValue.text = elevation.value.ToString("0.0");
        distanceValue.text = distance.value.ToString("0.0");
        transform.position = convertAngle(azimuth.value + azimuthOffset, elevation.value + elevationOffset, distance.value);
	}

    public Vector3 convertAngle(float azimuth, float elevation, float distance)
    {
        azimuth = convertToRadians(azimuth);
        elevation = convertToRadians(elevation);


        Vector3 newPosition;

        newPosition.x = distance * Mathf.Sin(elevation) * Mathf.Cos(azimuth);
        //Debug.Log("X: " + newPosition.x);
        newPosition.z = distance * Mathf.Sin(elevation) * Mathf.Sin(azimuth);
        //Debug.Log("Z: " + newPosition.z);
        newPosition.y = (distance * Mathf.Cos(elevation)) + listenerOffset;
        //Debug.Log("Y: " + newPosition.y);

        //Debug.Log(newPosition);
        return newPosition;
    }

    float convertToRadians(float angle)
    {
        float radian = angle * (Mathf.PI / 180);
        return radian;

    }

    public void ResetSliders(int a, int e, int d)
    {
        azimuth.value = a;
        elevation.value = e;
        distance.value = d;
    }
}
