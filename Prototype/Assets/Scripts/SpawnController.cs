﻿using UnityEngine;
using System.Collections;

public class SpawnController : MonoBehaviour
{
    public GameObject ball;
    public GameObject target;
    public Vector3 goalSpawnValues;
    public Object sourceInScene;
    GameObject[] goals;
    Vector3 spawnPosition = new Vector3(0, 1, 0);
    Quaternion spawnRotation = Quaternion.identity;
    

    void Awake()
    {
        //createBallSpawn();
	// passes GameObject over to Userfinished so it can use the transform
        gameObject.GetComponent<Userfinished>().soundSource = Instantiate(ball, spawnPosition, spawnRotation) as GameObject;
    }

    void Update()
    {
        goals = GameObject.FindGameObjectsWithTag("Goal");
        if (1 > goals.Length)
            spawnTargets();
    }

    void spawnTargets()
    {
        // detect how many targets are in the world
        Quaternion spawnRotation = Quaternion.identity;
        Vector3 goalSpawnPosition = new Vector3(Random.Range(-goalSpawnValues.x, goalSpawnValues.x), Random.Range(2, goalSpawnValues.y), Random.Range(-goalSpawnValues.z, goalSpawnValues.z));
        // if less than 1 spawn target at random location
        if (1 > goals.Length)
        {
            Instantiate(target, goalSpawnPosition, spawnRotation);
        }
    }

    /*public void createBallSpawn()
    {
        spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
        spawnRotation = Quaternion.identity;
    }*/
    public Vector3 getBallSpawnPosition()
    {
        return spawnPosition;
    }

    public Quaternion getBallSpawnRotation()
    {
        return spawnRotation;
    }

    public Vector3 randTargetPos(int azRes, int elevRes, float distRes, int index)
    {
        int azMin = azRes * index;
        int azMax = azMin + azRes;

        int elevMin = elevRes * index;
        int elevMax = elevMin + elevRes;

        float distMin = distRes * index;
        float distMax = distMin + distRes;

        float azimuth = Random.Range(azMin, azMax);
        float elevation = Random.Range(elevMin, elevMax);
        float distance = Random.Range(distMin, distMax);

        Rotate RotateObj = new Rotate();
        Vector3 position = RotateObj.convertAngle(azimuth, elevation, distance);
        return position;
    }
}
