﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadScene : MonoBehaviour {

    public Button introButton;
    public Button VRButton;
    public Button TwoDButton;
    public Button spatTest;

	// Use this for initialization
	void Start () {

        Cursor.visible = true;

        UnityEngine.VR.VRSettings.enabled = false;

        Button IntroButton= introButton.GetComponent<Button>();
        IntroButton.onClick.AddListener(LoadIntro);

        Button vrButton = VRButton.GetComponent<Button>();
        vrButton.onClick.AddListener(LoadVR);

        Button toDButton = TwoDButton.GetComponent<Button>();
        toDButton.onClick.AddListener(LoadtoD);

        spatTest = spatTest.GetComponent<Button>();
        spatTest.onClick.AddListener(LoadSpatTest);
    }

    void LoadIntro()
    {
        SceneManager.LoadScene("Intro", LoadSceneMode.Single);
    }
	
    void LoadVR()
    {
        SceneManager.LoadScene("Level3", LoadSceneMode.Single);
    }

    void LoadtoD()
    {
        SceneManager.LoadScene("Level1", LoadSceneMode.Single);
    }

    void LoadSpatTest()
    {
        SceneManager.LoadScene("SpatTest", LoadSceneMode.Single);
    }
}
