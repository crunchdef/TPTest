﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class introScript : MonoBehaviour {

    public GameObject instructions;
    GameObject soundSource;
    Text instructionsText;

    GameObject azimuthSlider;
    Slider azimuth;
    Text azimuthValue;
    GameObject elevationSlider;
    Slider elevation;
    Text elevationValue;
    GameObject distanceSlider;
    Slider distance;
    Text distanceValue;

    int dWaitT = 3;


    // Use this for initialization
    void Start () {
        instructionsText = instructions.GetComponent<Text>();
        soundSource = GameObject.FindGameObjectWithTag("SoundSource");

        // make the sound source visible
        MeshRenderer soundMesh = soundSource.GetComponent<MeshRenderer>();
        soundMesh.enabled = true;

        azimuthSlider = GameObject.Find("Azimuth");
        azimuth = azimuthSlider.GetComponent<Slider>();
        elevationSlider = GameObject.Find("Elevation");
        elevation = elevationSlider.GetComponent<Slider>();
        distanceSlider = GameObject.Find("Distance");
        distance = distanceSlider.GetComponent<Slider>();

        StartCoroutine(intro());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator WaitForInput(string buttonName)
    {
        while (!Input.GetButtonDown(buttonName))
        {
            yield return null;
        }
    }

    IEnumerator WaitForSlider(Slider slider, float target)
    {
    // the slider value goes 5 decimals but the the slider text rounds to 1 decimal place
        while (!(Mathf.Round(slider.value) == target))    // while 89.98914 is pretty bloody close it isn't *exactly* 90
        {
            yield return null;
        }
        instructionsText.text = "Perfect";
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator intro()
    {

        instructionsText.text = "Welcome to the 'tutorial'\n Click to continue";
        yield return StartCoroutine(WaitForInput("Fire1"));
        instructionsText.text = "You can move around by moving the mouse\n The sliders on the right control the sphere position \n Click";
        yield return StartCoroutine(WaitForInput("Fire1"));

        instructionsText.text = "Click the right mouse button to unlock the camera rotation from the mouse\n This will let you set adjust the sliders without moving the camera.\n Try it now";

        yield return StartCoroutine(WaitForInput("Fire2")); // wait until the player right clicks

        instructionsText.text = "Click again to lock the camera rotation to the mouse";
        yield return new WaitForSeconds(0.5f);   // for some reason needs a little break before it works again
        yield return StartCoroutine(WaitForInput("Fire2"));

        instructionsText.text = "We'll quickly run through the different sliders by moving each in turn\n Click to cont.";
        yield return StartCoroutine(WaitForInput("Fire1"));

        float distanceTarget = 15.0f;
        float elevationTarget = -27.0f;
        float azimuthTarget = 90.0f;

        /* Distance */
        instructionsText.text = "Change the distance slider to " + distanceTarget;
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(WaitForSlider(distance, distanceTarget));

        /* Elevation */
        instructionsText.text = "Change the elevation slider to " + elevationTarget;
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(WaitForSlider(elevation, elevationTarget));

        /* Azimuth */
        instructionsText.text = "Change the azimuth slider to " + azimuthTarget;
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(WaitForSlider(azimuth, azimuthTarget));

        instructionsText.text = "Now move the sphere to match the position of the target\n Click the scroll wheel when you're done";
        yield return StartCoroutine(WaitForInput("Fire3"));

        instructionsText.text = "Continue for the remaining targets\n Remember to keep listening to how it sounds";

        yield return null;     
    }

}
