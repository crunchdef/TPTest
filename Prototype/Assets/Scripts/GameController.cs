﻿ using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    [Header("Don't include file name")]
    public string resultsFilePath = "C:\\Users\\mjh\\Desktop\\";
    string resultsFile;
    System.IO.FileInfo rF;

    public GameObject ball;
    public GameObject target;
    public Vector3 goalSpawnValues;
    public Vector3 spawnPosition = new Vector3(0, 1, 0);
    public Quaternion spawnRotation = Quaternion.identity;
    public GameObject instructions;
    public GameObject testNumObj;

    public float azimuthResolution = 90;
    public float elevationResolution;
    public float distanceResolution;

    Rotate RotateObj;
    UnityStandardAssets.Characters.FirstPerson.MouseLook m_MouseLook;

    float startTime = 0;

    // List of possible target positions
    int[] azList = { 0, 45, 90, 135, 180, -135, -90, -45};
    int[] elevList = { 0, -50};
    float distList = 6.5f;

    //listener offset adjusts the source position to account for position of the listener so it rotates around the listner and not 0,0,0
    int listenerOffset = 1;
    //0 on the azimuth is off to the right for some reason. Probably maths but easier to just adjust it here
    int azimuthOffset = 90;
    // brings 0 to no elevation and 90 to directly above
    int elevationOffset = 90;

    int[] index;
    int indexIndex = 0;
    int elevIndex = 0;

    int testNumber = 1;
    int totalTestNum = 16;

    GameObject soundSource;
    GameObject targetObject;
    GameObject[] goals;

    Text instructionsText;
    Text testNumText;

    bool readyToFinish = false;
    bool finished = false;

    void Awake()
    {
        if (resultFile.filename == null)
        {
            resultFile.filename = resultFile.timeNow(); // if the filename hasn't been generated then do so
        }

	    resultsFilePath = System.Environment.ExpandEnvironmentVariables("%HOMEPATH%");
	    resultsFilePath = resultsFilePath + "\\Desktop\\MatthewFYPTests\\";
        if (!Directory.Exists(resultsFilePath))
        {
            Directory.CreateDirectory(resultsFilePath);
        }
        resultsFile = resultsFilePath + "test " + resultFile.filename + ".txt";

        /* 
         * Enable or disable VR support depending on level 
         * Also write section title in results file 
         */

        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Level1")
        {
            UnityEngine.VR.VRSettings.enabled = false;
            WriteSection("Desktop");
        }
        else if (scene.name == "Intro")
        {
            UnityEngine.VR.VRSettings.enabled = false;
            WriteSection("Intro");
        }
        else if (scene.name == "Level3")
        {
            UnityEngine.VR.VRSettings.enabled = true;
            Debug.Log("VRDevice: " + UnityEngine.VR.VRDevice.isPresent);
            WriteSection("VR");
        }

        instructionsText = instructions.GetComponent<Text>();
        testNumText = testNumObj.GetComponent<Text>();

        // passes GameObject over to Userfinished so it can use the transform
        calculateIndex();
        soundSource = Instantiate(ball, spawnPosition, spawnRotation) as GameObject;
        RotateObj = soundSource.GetComponent<Rotate>();

        WriteTitle();
        //Debug.Log(resultFile.filename);
    }

    // Use this for initialization
    void Start () {
        StartCoroutine(clickToBegin());
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown("Fire3"))
        {
            UserFinished();
        }
        if (Input.GetKeyDown("r"))
        {
            ResetCamera();
        }
    }

    // Rotation stuff //

    void calculateIndex()
    {
        // create an array with the element numbers in order
        index = new int[8] { 0, 1, 2, 3, 4, 5, 6, 7 };

        // shuffle the order of the array
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t <index.Length; t++)
        {
            int tmp = index[t];
            int r = Random.Range(t, index.Length);
            index[t] = index[r];
            index[r] = tmp;
        }  
        
        //DEBUG
        for (int i = 0; i < index.Length; i++)
        {
            Debug.Log(index[i]);
        } 

    }

    void incrementIndex()
    {
   /*     if (azIndex[0] < azIndex[1]-1)
        {
            azIndex[0]++;
        }
        else
        {
            azIndex[0] = 0;
            if (distIndex[0] < distIndex[1]-1)
            {
                distIndex[0]++;
            }
            else
            {
                distIndex[0] = 0;
                if (elevIndex[0] < elevIndex[1] - 1)
                {
                    elevIndex[0]++;
                }
                else
                {
                    Debug.Log("We're out of positions!");


                }
            }
        }*/
    }
    
    void spawnTargets()
    {
        Quaternion spawnRotation = Quaternion.identity;
        targetObject = Instantiate(target, randTargetPos(), spawnRotation) as GameObject;
        WritePositionToFile("Target", targetObject.transform.position);
        WritePositionToFile("Target AED: ", new Vector3(azList[index[indexIndex - 1]], elevList[elevIndex], distList));
    }

    public Vector3 getBallSpawnPosition()
    {
        return spawnPosition;
    }

    public Quaternion getBallSpawnRotation()
    {
        return spawnRotation;
    }

    public Vector3 randTargetPos()
    {
        //Rotate RotateObj = new Rotate();
        // spawn the target at the position of the current indexes
        Vector3 position = RotateObj.convertAngle(azList[index[indexIndex]] + azimuthOffset, 
            elevList[elevIndex] + elevationOffset, distList);

        // increment all the indexes
        // azimuth first, when it reaches max then increment elev
        
        if (indexIndex == azList.Length - 1)
        {
            indexIndex = 0;
            if (elevIndex == elevList.Length - 1) // if elevList is already at the max we've been through all the positions
            {
                //StartCoroutine(levelFinished());
                readyToFinish = true;
            }
            else
            {
                elevIndex++;
            }
        }
        else
        {
            indexIndex++;
        }
        return position;
    }

    // User triggered //

    void UserFinished()
    {
        if (!readyToFinish) // check if haven't got to the last target position
        {
            // waiting for user input to signify finished trial
            // then put the ball back in center
            //Rotate RotateObj = soundSource.GetComponent<Rotate>();
            EndTime();
            WritePositionToFile("Source: ", soundSource.transform.position);
            WritePositionToFile("Source AED: ", new Vector3(RotateObj.azimuth.value, RotateObj.elevation.value, RotateObj.distance.value));
            soundSource.transform.position = getBallSpawnPosition(); // reset sound source position
            soundSource.transform.rotation = getBallSpawnRotation(); // reset sound source rotation
            RotateObj.ResetSliders(0, 0, 1);

            WriteTitle();
            WritePositionToFile("Target AED: ", new Vector3(azList[index[indexIndex]], elevList[elevIndex], distList));
            targetObject.transform.position = randTargetPos();
            WritePositionToFile("Target", targetObject.transform.position);

            StartTime();
        }
        else if(readyToFinish && !finished)
        {
            finished = true;
            EndTime();
            WritePositionToFile("Source: ", soundSource.transform.position);
            WritePositionToFile("Source AED: ", new Vector3(RotateObj.azimuth.value, RotateObj.elevation.value, RotateObj.distance.value));
            StartCoroutine(levelFinished());
        }
    }

    void ResetCamera()
    {
        Camera camera = Camera.main;
        camera.transform.rotation = Quaternion.identity;
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        //UnityStandardAssets.Characters.FirstPerson.MouseLook.LookRotation(transform, camera.transform);
        //Debug.Log(camera.transform.position);
        m_MouseLook.LookRotation(player.transform, camera.transform);
    }

    void WritePositionToFile(string title, Vector3 position)
    {
        using (System.IO.StreamWriter file =
        new System.IO.StreamWriter(@resultsFile, true))
        {
            file.WriteLine(title + position);
        }
    }

    void WriteSection(string section)
    {
        using (System.IO.StreamWriter file =
        new System.IO.StreamWriter(@resultsFile, true))
        {
            file.WriteLine('@' + section + '@');
        }
        
    }

    void WriteTitle()
    {
            testNumText.text = "Test:" + testNumber + "/16"; //test number to appear on screen
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@resultsFile, true))
            {
                file.WriteLine("^Test " + testNumber + '^');
            }
            testNumber++;
    }

    void StartTime()
    {

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@resultsFile, true))
            {
                file.WriteLine("Start: " + Time.time);
            }
            startTime = Time.time;

    }

    void EndTime()
    {
        float timeTaken;
        using (System.IO.StreamWriter file =
        new System.IO.StreamWriter(@resultsFile, true))
        {
            file.WriteLine("End: " + Time.time);
        }
        timeTaken = Time.time - startTime;
        using (System.IO.StreamWriter file =
        new System.IO.StreamWriter(@resultsFile, true))
        {
            file.WriteLine("Taken: " + timeTaken);
        }
    }

    IEnumerator clickToBegin()
    {
        instructionsText.text = "Click to begin";
        while (!Input.GetButtonDown("Fire1"))
        {
            yield return null;
        }
        instructionsText.text = " ";
        spawnTargets();
        StartTime();
    }

    IEnumerator levelFinished()
    {
        instructionsText.text = "Level finished, click to return to the main menu";
        while (!Input.GetButtonDown("Fire1"))
        {
            yield return null;
        }
        SceneManager.LoadScene("LevelSelect", LoadSceneMode.Single);    
    }
}
