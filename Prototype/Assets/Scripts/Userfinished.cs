﻿using UnityEngine;
using System.Collections;

public class Userfinished : MonoBehaviour {
    
    SpawnController controller = new SpawnController();
    public GameObject soundSource; // gets assigned directly from SpawnController.cs
    
    // Update is called once per frame
    void Update () {
	if (Input.GetButtonDown("Fire3"))
        {
            Debug.Log("Source position: " + soundSource.transform.position);
	        soundSource.transform.position = controller.getBallSpawnPosition(); // reset sound source position
	        soundSource.transform.rotation = controller.getBallSpawnRotation(); // reset sound source rotation
        }
    }
}
